# Global Handler

This project is a exceptions handler. This artifact detects when an exception is throwed and make a standard rest response.

### Install 

To install in local repository use this command:

```bash
    $ mvn clean install
```


### Add dependecy

To use the artifacts of this project include the following dependency in `pom.xml`.

```xml
    <dependency>
        <groupId>ar.com.pablocaamano</groupId>
        <artifactId>global-handler</artifactId>
        <version>0.1-SNAPSHOT</version>
    </dependecy>
```