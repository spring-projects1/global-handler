package ar.com.pablocaamano.handler;

import ar.com.pablocaamano.commons.builder.ResponseBuilder;
import ar.com.pablocaamano.commons.exception.http.CommonHttpException;
import ar.com.pablocaamano.commons.rest.Error;
import ar.com.pablocaamano.commons.rest.RestResponse;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import javax.servlet.http.HttpServletRequest;

@ControllerAdvice("ar.com.pablocaamano")
@Order(Ordered.HIGHEST_PRECEDENCE)
public class GlobalHandler {

    @ExceptionHandler({CommonHttpException.class})
    protected ResponseEntity<RestResponse> handCommonException(
            CommonHttpException commonException,
            HttpServletRequest request
    ){
        Error error = new Error();
        error.setCode(commonException.getCode());
        error.setCause(commonException.getCause());
        error.setDescription(commonException.getMessage());
        return new ResponseEntity<>(
                ResponseBuilder.init()
                        .addRequestMethod(request.getMethod())
                        .addRequestUri(request.getRequestURI())
                /** @TODO complete this attribute */
                        .addTimestamp("")
                        .addError(error)
                        .build(),
                commonException.getStatus()
        );
    }

    @ExceptionHandler({Exception.class})
    protected ResponseEntity<RestResponse> handAnyException(
            Exception exception,
            HttpServletRequest request
    ){
        HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        Error error = new Error();
        error.setDescription(exception.getMessage());
        if (exception instanceof CommonHttpException){
            CommonHttpException commonException = (CommonHttpException) exception;
            error.setCode(commonException.getCode());
            error.setCause(commonException.getCause());
            error.setDescription(commonException.getMessage());
            httpStatus = commonException.getStatus();
        }
        return new ResponseEntity<>(
                ResponseBuilder.init()
                        .addRequestMethod(request.getMethod())
                        .addRequestUri(request.getRequestURI())
                /** @TODO complete this attribute */
                        .addTimestamp("")
                        .addError(error)
                        .build(),
                httpStatus
        );
    }

}
