package ar.com.pablocaamano.handler.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.DispatcherServlet;
import javax.annotation.PostConstruct;

@Configuration
@ComponentScan("ar.com.pablocaamano")
@PropertySource({ "classpath:application.properties" })

public class HandlerConfiguration {

    @Autowired
    private DispatcherServlet dispatcherServlet;

    @PostConstruct
    public void initialize(){
        dispatcherServlet.setThrowExceptionIfNoHandlerFound(true);
    }

}
